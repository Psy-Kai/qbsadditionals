# QbsAdditionals

Contains common `Items` and `Modules` for building qbs projects.

To use the content of this repository you need to set the `preferences.qbsSearchPaths`. For this got to Tools->Options->Kits->Kits select the kit you want to configure and change "Additional Qbs Profile Settings". As key pass `preferences.qbsSearchPaths` and as value pass an array with the path to the root directory of this repository on your disk.

import qbs
import qbs.FileInfo

Module {
    property string outputFileType: "merged";
    PropertyOptions {
        name: "outputFileType"
        description: "Defines the file type created by repc."
        allowedValues: [
            "source",
            "replica",
            "merged",
        ]
    }

    FileTagger {
        patterns: ["*.rep"]
        fileTags: ["repc-rep"]
    }

    Rule {
        inputs: ["repc-rep"]
        Artifact {
            filePath: {
                var basePath = product.sourceDirectory;
                var fileType = product.repc.outputFileType;
                return FileInfo.joinPaths(basePath, "repc_" + FileInfo.baseName(input.fileName) +
                                          "_" + fileType + ".h");
            }
            fileTags: ["hpp", "repc_hpp"]
        }
        prepare: {
            var cmd = new Command();
            cmd.description = "repc " + input.fileName;
            cmd.program = FileInfo.joinPaths(product.Qt.core.binPath, "repc");
            var fileType = product.repc.outputFileType;
            cmd.arguments = ["-i", "rep", "-o", fileType, input.filePath, output.filePath];
            return cmd;
        }
    }
}
